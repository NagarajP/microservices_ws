package com.myzee.movieratingservice;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ratings")
public class MovieRatingController {
	
	@RequestMapping("/{movieId}")
	public Rating getRating(@PathVariable String movieId) {
		return new Rating(movieId, 4);
	}
	
	@RequestMapping("/user/{userId}")
	public UserRating getUserRatings(@PathVariable String userId) {
		List<Rating> ratings = Arrays.asList(new Rating("nvnv", 5),new Rating("tzp", 4));
		UserRating userrate = new UserRating();
		userrate.setRatings(ratings);
		return userrate;
	}
}
